/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotizacion01;

/**
 *
 * @author User
 */
public class cot02 {
    public static void main(String[] args) {
        // crear objeto de cotizacion por omisión
        Cotizacion01 cotizacionPorOmicion = new Cotizacion01();
        
        cotizacionPorOmicion.setDescripAuto("Nissan");
        cotizacionPorOmicion.setNumCotizacion(123);
        cotizacionPorOmicion.setPlazo(36);
        cotizacionPorOmicion.setPorcentajeImpuesto(25);
        cotizacionPorOmicion.setPrecio(220000);

        // mostrar información del objeto
        System.out.println("Cotización por Omisión");
        System.out.println("Número de Cotización: " + cotizacionPorOmicion.getNumCotizacion());
        System.out.println("Descripción del Auto: " + cotizacionPorOmicion.getDescripAuto());
        System.out.println("Precio: " + cotizacionPorOmicion.getPrecio());
        System.out.println("Porcentaje de Impuesto: " + cotizacionPorOmicion.getPorcentajeImpuesto());
        System.out.println("Plazo: " + cotizacionPorOmicion.getPlazo());

        // Calcular y mostrar el pago de impuesto, financiamiento y mensualidad 
        System.out.println("Pago de Impuesto: " + cotizacionPorOmicion.calcularPagoImpuesto());
        System.out.println("Financiamiento: " + cotizacionPorOmicion.calcularFinanciamiento());
        System.out.println("Mensualidad: " + cotizacionPorOmicion.calcularMensualidad());
    }
}
    
