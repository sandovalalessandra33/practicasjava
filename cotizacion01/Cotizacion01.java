/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotizacion01;

/**
 *
 * @author User
 */
public class Cotizacion01 {

            // atributos de la clase
    private int numCotizacion;
    private String descripAuto;
    private float precio;
    private float porcentajeImpuesto;
    private int plazo;

    // constructores
    public cotizacion() {
        this.numCotizacion=0;
        this.descripAuto = "";
        this.precio = 0.0f;
        this.porcentajeImpuesto = 0.0f;
        this.plazo = 0;
    }

    public cotizacion(int numCotizacion, String descripAuto, float precio, float porcentajeImpuesto, int plazo) {
        this.numCotizacion = numCotizacion;
        this.descripAuto = descripAuto;
        this.precio = precio;
        this.porcentajeImpuesto = porcentajeImpuesto;
        this.plazo = plazo;
    }

    // accesos (gets y sets)
    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripAuto() {
        return descripAuto;
    }

    public void setDescripAuto(String descripAuto) {
        this.descripAuto = descripAuto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(float porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    // metodos de comportamiento
    public double calcularPagoImpuesto() {
        // calcula el impuesto con el precio y el porcentaje de impuesto
        return precio * (porcentajeImpuesto / 100);
    }

    public double calcularFinanciamiento() {
        // calcula el financiamiento restando el impuesto del precio
        return precio - calcularPagoImpuesto();
    }

    public double calcularMensualidad() {
        // calcula la mensualidad dividiendo el financiamiento entre el plazo
        return calcularFinanciamiento() / plazo;
    }
    
}
