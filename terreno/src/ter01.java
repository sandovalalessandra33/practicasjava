/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class ter01 {
    //atributos de la clase
    private int numTerreno;
    private float ancho;
    private float largo;
    
    //metodos 
    
    //metodos contructores
    
    //omicion
    
    public ter01(){
        this.numTerreno=0;
        this.ancho=0.0f;
        this.largo=0.0f;
    }
    
    //argumentos
    
    public ter01( int numTerreno, float ancho, float largo){
        
        this.numTerreno = numTerreno;
        this.ancho = ancho;
        this.largo = largo;
        
    }
    
    //Copia
    
    public ter01(ter01 otro){
        
        this.numTerreno = otro.numTerreno;
        this.ancho = otro.ancho;
        this.largo = otro.largo;
        
    }

    public int getNumTerreno() {
        return numTerreno;
    }

    public void setNumTerreno(int numTerreno) {
        this.numTerreno = numTerreno;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }
    
    //metodos de comportamiento
    
    public float calcularPerimetro(){
        
        float perimetro = 0.0f;
        perimetro = (this.ancho + this.largo)*2;
        return perimetro;
        
    }
    
    public float calcularArea(){
        float area =0.0f;
        area = this.ancho * this.largo;
        return area;
}
}