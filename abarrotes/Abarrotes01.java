/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Abarrotes01 {
     // atributos de la clase
    private int codProducto;
    private String descripcion;
    private String unidadMedida;
    private float precioC;
    private float precioV;
    private int cantidadProducto;
    
    // atributos de la clase
    public Abarrotes01(){
    this.codProducto = 0;
    this.descripcion = "";
    this.unidadMedida = "";
    this.precioC = 0.0f;
    this.precioV = 0.0f;
    this.cantidadProducto = 0;
    
    
    }
    
    //contructores
    public Abarrotes01(int codProducto, String descripcion, String unidadMedida, float precioC, float precioV, int cantidadProducto){
    this.codProducto = codProducto;
    this.descripcion = descripcion;
    this.unidadMedida = unidadMedida;
    this.precioC = precioC;
    this.precioV = precioV;
    this.cantidadProducto = cantidadProducto;
    }
    
    //accesos y sets y gets.
    public int getCodProduct() {
        return codProducto;
    }
    public void setCodProduct(int codProduct){
        this.codProducto = codProduct;
    }
    public String getDescripcion(){
        return descripcion;
    }
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
    public String getUnidadMedida(){
        return unidadMedida;
    }
    public void setUnidadMedida(String unidadMedida){
        this.unidadMedida = unidadMedida;
    }
    public float getPrecioC(float precioC){
        return precioC;
    }
    public void setPrecioC(float precioC){
        this.precioC = precioC;
    }
    public float getPrecioV(){
        return precioV;
    }
    public void setPrecioV(float precioV){
        this.precioV = precioV;
    }
    public int cantidadProducto(){
        return cantidadProducto;
    }
    public void setCantidadProducto(int cantidadProducto){
        this.cantidadProducto = cantidadProducto;
    }
    //Metodos de comportamiento
    public double calcularPrecioV(){
        return precioV * cantidadProducto;
    }
    public double calcularPrecioC(){
        return precioC * cantidadProducto;
    }
    public double calcularGanancia(){
        double precioCompraTotal = calcularPrecioC();
        double precioVentaTotal = calcularPrecioV();
    return precioVentaTotal - precioCompraTotal;
    }
}
